import React from 'react';
import Seo from '../components/SEO/SEO';
import { StaticQuery, graphql } from 'gatsby';
import Layout from '../components/Layout/Layout';
// import Hero from '../components/Hero/Hero';
// import CtaArea from '../components/CTAArea/CTAArea';
// import LatestBlogPost from '../components/LatestBlogPost/LatestBlogPost';
// import Quote from '../components/Quote/Quote';
// import About from '../components/About/About';

const IndexPage = ({ data }) => {
  const { wpPage } = data;
  const page = wpPage;
  const { seo } = page;
  return (
    <>
      <Seo seoData={seo} uri={page.uri} />
      <Layout>
        {/* <Seo title="Home" /> */}
        {/* <Hero />
    <CtaArea />
    <LatestBlogPost />
    <Quote />
    <About /> */}
        <div className={`pageWrapper`}>
          <div className={`ContentWrapper ${page.title} page`}>
            <article className={`pageContent`}>
              <h1 dangerouslySetInnerHTML={{ __html: page.title }} />
              <div dangerouslySetInnerHTML={{ __html: page.content }} />
            </article>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default IndexPage;
export const pageQuery = graphql`
  query {
    wpPage(uri: { eq: "/" }) {
      id
      title
      content
      status
      featuredImage {
        node {
          id
          localFile {
            childImageSharp {
              gatsbyImageData(width: 1920, placeholder: TRACED_SVG)
            }
          }
        }
      }
      seo {
        ...SeoData
      }
      # wpChildren {
      #   nodes {
      #     ... on WpPage {
      #       id
      #       uri
      #       title
      #     }
      #   }
      # }
      # wpParent {
      #   node {
      #     ... on WpPage {
      #       id
      #       uri
      #       title
      #       wpChildren {
      #         nodes {
      #           ... on WpPage {
      #             id
      #             title
      #             uri
      #           }
      #         }
      #       }
      #     }
      #   }
      # }
    }
  }
`;
