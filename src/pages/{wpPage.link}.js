import React from 'react';
import { graphql } from 'gatsby';
//import styled from 'styled-components';
//import classNames from 'classnames';
import Layout from '../components/Layout/Layout';
import Seo from '../components/SEO/SEO';

// import PageHero from '../components/PageHero/PageHero';
// import BreadCrumb from '../components/BreadCrumb/BreadCrumb';
// import PageSidebar from '../components/PageSidebar/PageSidebar';

// const ContentWrapper = styled.div`
//   display: block;
//   @media (min-width: 992px) {
//     display: flex;
//   }
// `;

const PageTemplate = ({ data }) => {
  const { wpPage } = data;
  const page = wpPage;
  const { seo } = page;
  return (
    <>
      <Seo seoData={seo} uri={page.uri} />
      <Layout>
        {/*  {data.wpPage.featuredImage ? (
       <PageHero
        img={
          data.wpPage.featuredImage.node.localFile.childImageSharp
            .gatsbyImageData
        }
      />
    ) : null}
    <div classname={Wrapper}>
      <BreadCrumb parent={data.wpPage.wpParent && data.wpPage.wpParent.node} />
      <ContentWrapper>
        <PageSidebar
          parentChildren={
            data.wpPage.wpParent && data.wpPage.wpParent.node.wpChildren.nodes
          }
          currentPage={data.wpPage}
          parent={data.wpPage.wpParent && data.wpPage.wpParent.node.title}
        >
          {data.wpPage.wpChildren}
        </PageSidebar>
        <PageContent>
          <h1 dangerouslySetInnerHTML={{ __html: data.wpPage.title }} />
          <div dangerouslySetInnerHTML={{ __html: data.wpPage.content }} />
        </PageContent>
      </ContentWrapper>
    </div> */}
        <div className={`pageWrapper`}>
          <div className={`ContentWrapper ${page.title} page`}>
            <article className={`pageContent`}>
              <h1 dangerouslySetInnerHTML={{ __html: page.title }} />
              <div dangerouslySetInnerHTML={{ __html: page.content }} />
            </article>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default PageTemplate;

export const pageQuery = graphql`
  query ($id: String!) {
    wpPage(id: { eq: $id }) {
      id
      title
      content
      status
      featuredImage {
        node {
          id
          localFile {
            childImageSharp {
              gatsbyImageData(width: 1920, placeholder: TRACED_SVG)
            }
          }
        }
      }
      seo {
        ...SeoData
      }
      wpChildren {
        nodes {
          ... on WpPage {
            id
            uri
            title
          }
        }
      }
      wpParent {
        node {
          ... on WpPage {
            id
            uri
            title
            wpChildren {
              nodes {
                ... on WpPage {
                  id
                  title
                  uri
                }
              }
            }
          }
        }
      }
    }
  }
`;
