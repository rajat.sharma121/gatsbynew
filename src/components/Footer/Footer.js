import React from 'react';
import { wrapper } from './Footer.module.scss';

const Footer = () => (
  <footer className={wrapper}>
    <p>Do you want to get updates and news? Contact us on the adress below.</p>
    <p>test! | test test 10 | test test | test@test.com</p>
  </footer>
);

export default Footer;
