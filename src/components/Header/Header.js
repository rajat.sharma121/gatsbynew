import React from 'react';
import { Link } from 'gatsby';
import Navigation from '../Navigation/Navigation';
import Logo from '../../images/logo.svg';
import { useMenuQuery } from '../../hooks/useMenuQuery';
import { wrapper, content } from './Header.module.scss';

const Header = () => {
  const { site, menu } = useMenuQuery();

  return (
    <header className={wrapper}>
      <div className={content}>
        <Link to="/">
          <img src={Logo} alt={site.siteMetadata.title} />
        </Link>
        <Navigation menu={menu.menuItems.nodes} />
      </div>
    </header>
  );
};

export default Header;
