import React from 'react';
import { HamburgerButton } from './Hamburger.module.scss';
import HamburgerIcon from '../../images/menu-icon.svg';

const Hamburger = ({ handleOverlayMenu }) => (
  <div
    className={HamburgerButton}
    onClick={handleOverlayMenu}
    aria-hidden="true"
  >
    <img src={HamburgerIcon} alt="menu-hamburger" />
  </div>
);

export default Hamburger;
