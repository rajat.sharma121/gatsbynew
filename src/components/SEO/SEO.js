/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { useStaticQuery, graphql } from 'gatsby';

function SEO({ meta, seoData, dateGmt, modifiedGmt, uri }) {
  const { site, wp } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            siteUrl
          }
        }
        wp {
          seo {
            openGraph {
              defaultImage {
                localFile {
                  publicURL
                }
                mediaDetails {
                  width
                  height
                }
                altText
              }
            }
            social {
              twitter {
                cardType
              }
            }
          }
        }
      }
    `
  );

  if (!seoData) return null;

  const {
    canonical,
    metaDesc,
    opengraphDescription,
    opengraphImage,
    //opengraphSiteName,
    opengraphTitle,
    opengraphType,
    title,
    twitterDescription,
    twitterTitle,
    twitterImage,
  } = seoData;

  const siteDefaultMetadata = site.siteMetadata;
  const lang = `en`;
  const { siteUrl, title: defaultTitle } = siteDefaultMetadata;
  const fallBackImage =
    opengraphImage || twitterImage || wp.seo.openGraph?.defaultImage;
  let finalCanonical = siteUrl + uri;

  if (canonical && uri !== '/') {
    finalCanonical = canonical.startsWith('http')
      ? canonical
      : siteUrl + canonical;
  }
  const metaDescription = metaDesc || site.siteMetadata.description;
  const ogUrl = finalCanonical;
  const articleMeta =
    opengraphType === 'article' && dateGmt
      ? [
          {
            name: 'article:published_time',
            content: dateGmt,
          },
          {
            name: 'article:modified_time',
            content: modifiedGmt,
          },
          {
            name: 'og:updated_time',
            content: modifiedGmt,
          },
        ]
      : [];

  const imageMeta = fallBackImage?.localFile
    ? [
        {
          property: 'og:image',
          content: siteUrl + fallBackImage.localFile.publicURL,
        },
        {
          property: 'og:image:width',
          content: fallBackImage.mediaDetails.width,
        },
        {
          property: 'og:image:height',
          content: fallBackImage.mediaDetails.height,
        },
        {
          name: 'twitter:image',
          content:
            siteUrl +
            (twitterImage?.localFile?.publicURL ||
              fallBackImage.localFile.publicURL),
        },
        {
          name: 'twitter:image:width',
          content:
            twitterImage?.mediaDetails.width ||
            fallBackImage.mediaDetails.width,
        },
        {
          name: 'twitter:image:height',
          content:
            twitterImage?.mediaDetails.height ||
            fallBackImage.mediaDetails.height,
        },
        {
          name: 'twitter:image:alt',
          content: twitterImage?.altText || fallBackImage.altText,
        },
      ]
    : [];

  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      link={
        finalCanonical
          ? [
              {
                rel: 'canonical',
                href: finalCanonical,
              },
            ]
          : []
      }
      title={title || defaultTitle}
      meta={[
        {
          name: `title`,
          content: title,
        },
        {
          name: 'google-site-verification',
          content: 'N41HlOqnlgd2AyjlubZKx9YpXFYT5mOF0UzaiuAiHP4',
        },
        {
          name: 'yandex-verification',
          content: 'ea0e503699f8faae',
        },
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:locale`,
          content: `en_US`,
        },
        {
          property: `og:url`,
          content: ogUrl,
        },
        {
          property: `og:site_name`,
          content: `Gatsby`,
        },
        {
          property: `og:title`,
          content: opengraphTitle || title,
        },
        {
          property: `og:description`,
          content: opengraphDescription || metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: wp.seo?.social?.twitter?.cardType || 'summary_large_image',
        },
        {
          name: `twitter:title`,
          content: twitterTitle || title,
        },
        {
          name: `twitter:description`,
          content: twitterDescription || metaDescription,
        },
      ]
        .concat(imageMeta)
        .concat(articleMeta)
        .concat(meta)}
    />
  );
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
  uri: '',
  seo: {},
};
export default SEO;
