import React from 'react';
import { Link } from 'gatsby';
import InvertedLogo from '../../images/logo-inverted.svg';
import CloseButton from '../../images/close_btn.svg';
import { useMenuQuery } from '../../hooks/useMenuQuery';
import {
  Overlay,
  closeButton,
  overlayMenu,
  invertedLogo,
  inner,
  overlayActive,
} from './OverlayMenu.module.scss';

const OverlayMenu = ({ menuopen, callback }) => {
  const { menu } = useMenuQuery();
  return (
    <div
      className={Overlay}
      menuopen={menuopen}
      style={{
        opacity: menuopen ? '1' : '0',
        transform: menuopen ? 'translateX(0%)' : 'translateX(-100%)',
      }}
    >
      <div className={inner}>
        <img className={invertedLogo} src={InvertedLogo} alt="white-logo" />
        <ul className={overlayMenu}>
          {menu.menuItems.nodes.map((item) =>
            !item.parentId ? (
              <li key={item.id}>
                <Link to={item.url} activeClassName={overlayActive}>
                  {item.label}
                </Link>
              </li>
            ) : null
          )}
        </ul>
        <div
          className={closeButton}
          onClick={callback}
          role="button"
          tabIndex="0"
          onKeyDown={callback}
          aria-hidden="true"
        >
          <img src={CloseButton} alt="close-button" />
        </div>
      </div>
    </div>
  );
};

export default OverlayMenu;
