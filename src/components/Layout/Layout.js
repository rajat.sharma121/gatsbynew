import React, { useState } from 'react';
import Header from '../Header/Header';
import Hamburger from '../Hamburger/Hamburger';
import OverlayMenu from '../OverlayMenu/OverlayMenu';
import Footer from '../Footer/Footer';
//import * as LayoutStyle from './Layout.module.scss';
import './Layout.module.scss';
const Layout = ({ children }) => {
  const [menuOpen, setMenuOpen] = useState(0);
  const handleOverlayMenu = () => setMenuOpen((prev) => (!prev ? 1 : 0));
  return (
    <>
      <Hamburger handleOverlayMenu={handleOverlayMenu} />
      <OverlayMenu menuopen={menuOpen} callback={handleOverlayMenu} />
      <Header />
      <main className={`primary`}>{children}</main>
      <Footer />
    </>
  );
};

export default Layout;
