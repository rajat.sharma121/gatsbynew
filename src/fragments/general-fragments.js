import { graphql } from 'gatsby';

export const generalFragments = graphql`
  fragment SeoData on WpPostTypeSEO {
    breadcrumbs {
      text
      url
    }
    canonical
    metaDesc
    opengraphDescription
    opengraphImage {
      localFile {
        publicURL
      }
      mediaDetails {
        width
        height
      }
      altText
    }
    opengraphSiteName
    opengraphTitle
    opengraphType
    title
    twitterDescription
    twitterTitle
    twitterImage {
      localFile {
        publicURL
      }
      mediaDetails {
        width
        height
      }
      altText
    }
  }

  fragment PagePreview on WpPage {
    title
    uri
    baseFields {
      previewDescription
      previewImage {
        ...ThumbnailImage
      }
    }
    featuredImage {
      node {
        ...ThumbnailImage
      }
    }
  }

  fragment PostPreview on WpPost {
    title
    uri
    baseFields {
      previewDescription
      previewImage {
        ...ThumbnailImage
      }
    }
    featuredImage {
      node {
        ...ThumbnailImage
      }
    }
  }

  fragment WorkPreview on WpWork {
    title
    uri
    baseFields {
      previewDescription
      previewImage {
        ...ThumbnailImage
      }
    }
    featuredImage {
      node {
        ...ThumbnailImage
      }
    }
  }

  fragment IdeaPreview on WpIdea {
    title
    uri
    baseFields {
      previewDescription
      previewImage {
        ...ThumbnailImage
      }
    }
    featuredImage {
      node {
        ...ThumbnailImage
      }
    }
  }

  fragment ACFLink on WpACF_Link {
    target
    title
    url
  }

  fragment BackendSettings on Wp {
    gatsbyWpBackendSettings {
      gatsbyWpBackendSettings {
        footerCallout {
          callToAction {
            target
            title
            url
          }
          heading
          description
          show
        }
        footerColumnFour {
          heading
          links {
            link {
              target
              title
              url
            }
          }
        }
        footerColumnThree {
          heading
          links {
            link {
              target
              title
              url
            }
          }
        }
        footerColumnTwo {
          heading
          links {
            link {
              title
              target
              url
            }
          }
        }
      }
    }
  }
`;
