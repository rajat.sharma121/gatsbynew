import { graphql } from "gatsby"

export const imageFragments = graphql`
  fragment FluidImage on WpMediaItem {
    altText
    localFile {
      publicURL
      extension
      childImageSharp {
        fluid(quality: 90) {
          ...GatsbyImageSharpFluid_withWebp
          presentationWidth
        }
      }
    }
  }
  fragment ThumbnailImage on WpMediaItem {
    altText
    localFile {
      childImageSharp {
        fluid(quality: 90, maxWidth: 800) {
          ...GatsbyImageSharpFluid_withWebp
          presentationWidth
        }
      }
    }
  }

  fragment Icon on WpMediaItem {
    altText
    localFile {
      childImageSharp {
        fluid(maxWidth: 400, quality: 100) {
          ...GatsbyImageSharpFluid_withWebp_noBase64
          presentationWidth
        }
      }
    }
  }

  fragment HeroImage on WpMediaItem {
    altText
    localFile {
      childImageSharp {
        fluid(maxWidth: 1920, quality: 90) {
          ...GatsbyImageSharpFluid_withWebp
          presentationWidth
        }
      }
    }
  }

  fragment HeroImageMobile on WpMediaItem {
    altText
    localFile {
      childImageSharp {
        fluid(maxWidth: 750, quality: 90) {
          ...GatsbyImageSharpFluid_withWebp
          presentationWidth
        }
      }
    }
  }
  fragment IdeaHeaderImage on WpMediaItem {
    altText
    localFile {
      childImageSharp {
        fluid(maxWidth: 975, quality: 90) {
          ...GatsbyImageSharpFluid_withWebp
          presentationWidth
        }
      }
    }
  }

  fragment AvatarImage on File {
    childImageSharp {
      fluid(maxWidth: 300) {
        ...GatsbyImageSharpFluid_withWebp_tracedSVG
      }
    }
  }
`
