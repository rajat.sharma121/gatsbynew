// require('dotenv').config({
//   path: `.env`,
// });

//console.info('Environment:', process.env.NODE_ENV);
module.exports = {
  pathPrefix: `/gatsbynew/`,
  siteMetadata: {
    siteUrl: `https://rajat.sharma121.gitlab.io/gatsbynew/`,
    title: 'WPGatsby',
  },
  plugins: [
    {
      resolve: 'gatsby-source-wordpress',
      options: {
        url: `https://dev-wpgatsbydev.pantheonsite.io/graphql`,
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-image',
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: {
        trackingId: 'UA-000000-2',
      },
    },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sitemap',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `./src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: './src/images/',
      },
      __key: 'images',
    },
  ],
};
